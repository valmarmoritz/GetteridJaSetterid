﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetteridJaSetterid
{
    class Program
    {
        static void Main(string[] args)
        {
            Test t1 = new Test { Nimi = "Henn", Vanus = 61 };
            //Test t1 = new Test { Nimi = "Henn" };
            t1.setVanus(61);
            Test t2 = t1;
            t2.Nimi = "ANTS";
            Console.WriteLine(t1+"; "+t2);

            StrTest t3 = new StrTest { Nimi = "Henn", Vanus = 61 };
            StrTest t4 = t3;
            t4.Nimi = "Ants";
            Console.WriteLine(t3.Nimi + "; " + t4.Nimi);

            t1.Nimi = "sotapota";
            t1.setVanus(-7);
            Console.WriteLine(t1);
            t1.Vanus = -7;
            Console.WriteLine(t1);
            t1.setVanus(7);
            Console.WriteLine(t1);
            t1.Vanus = 17;
            Console.WriteLine(t1);

            int i = 3;
            int j = 4;
            Console.WriteLine(i+", "+j);
            Imelik(ref i, ref j);
            Console.WriteLine(i+", "+j);
            Imelik(ref i, ref i);                               // siin antakse mõlemaks parameetriks sama aadress, muudetakse "mõlemat", st ikka ainult i-d
            Console.WriteLine(i+", "+j);

        }

        public static void Imelik(ref int x, ref int y)         // antakse ette aadress, mitte väärtus
        {
            x++;
            y++;
        }
    }

    class Test                          // class tehakse Heapi, omistamine tähendab aadressi andmist
    {
        // public string Nimi;
        private string _Nimi;
        // public int Vanus;
        private int _Vanus;
        private int _Pikkus;
        public string Nimi { get { return _Nimi; } set { _Nimi = value.Substring(0, 1).ToUpper() + value.Substring(1).ToLower(); } }
        public void setVanus(int x)
        {
            if (x > 0) _Vanus = x;
        }
        public int getVanus() { return _Vanus; }
        public int Vanus                                // property
        {
            get { return _Vanus; }
            set { if (value > 0) _Vanus = value; }
        }
        public int Pikkus
        {
            get => _Pikkus;                             // kirjutatud lambda-avaldiste kaudu
            set => _Pikkus = value > 0 ? value : 0;
        }
        public string Linn { get; set; }                // property, mille sisemistel muutujatel nimesid ei olegi
        public string Saladus { get; private set; }     // property, mille väärtust saab muuta ainult klassi seest

        public Test() { }                               // konstruktori overloadimine, et saaks luua uut objekti ilma parameetriteta
        public Test(string nimi, int vanus)             // konstruktor
        {
            Nimi = nimi; Vanus = vanus;                 // väärtuste omistamine property'de kaudu, mis tagab sisse kirjutatud ärireeglite täitmise
        }
        public override string ToString()
        {
            return $"{Nimi}, {_Vanus} aastane";
        }
    }
    struct StrTest                      // struct tehakse Stacki, igale muutujale oma aadress, omistamine tähendab väärtustamist
    {
        public string Nimi;
        public int Vanus;
    }

}
