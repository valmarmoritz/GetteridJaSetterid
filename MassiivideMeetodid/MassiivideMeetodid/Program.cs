﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MassiivideMeetodid
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arvud = {1,2,6,2,9,2,12,5,1,2,0,10};       // Array-ga (massiiviga) ei ole väga hea tööd teha, nt ei saa sellesse lisada ega sealt ära võtta, parem on töötada listiga

            foreach (var x in arvud)
            {
                Console.Write(x + " ");
            }
            Console.WriteLine();

            arvud.ToList().ForEach(x => Console.Write(x + " ")); Console.WriteLine();       // eelmine tsükkel lühemalt, tehes masiivi enne listiks, millel on meetod ForEach

            Console.WriteLine($"summa: {arvud.Sum()}");
            Console.WriteLine($"tükki: {arvud.Count()}");
            Console.WriteLine($"keskmine: {arvud.Average()} = {arvud.Sum() / (double)arvud.Count()}");

            List<int> arvudelist = new List<int>(arvud);
            var larvud = arvud.ToList();

            var suurteArvudeList = arvud.ToList().Where(x => x > 5);

            Console.Write("sorteeritud, ilma algandmeid muutmata: ");
            arvud.OrderBy(x => x).ToList().ForEach(x => Console.Write(x + " ")); Console.WriteLine();
            Console.Write("  tagurpidi, ilma algandmeid muutmata: ");
            arvud.OrderBy(x => -x).ToList().ForEach(x => Console.Write(x + " ")); Console.WriteLine();
            Console.Write("algne järjestus on säilunud: ");
            arvud.ToList().ForEach(x => Console.Write(x + " ")); Console.WriteLine();


            Console.Write("sorteeritud: ");
            Array.Sort(arvud);
            arvud.ToList().ForEach(x => Console.Write(x + " ")); Console.WriteLine();

            Console.Write("  tagurpidi: ");
            Array.Reverse(arvud);
            arvud.ToList().ForEach(x => Console.Write(x + " ")); Console.WriteLine();


            Console.WriteLine();
            var ruudud = arvud.Distinct().OrderBy(x => x).ToDictionary(x => x, x => x * x);
            foreach (var x in ruudud.Keys) Console.WriteLine($"{x} ruut on {ruudud[x]}");


            Console.WriteLine(  );
            arvud.OrderBy(x => x).ToList().ForEach(x => Console.Write(x + " ")); Console.WriteLine();

            Console.WriteLine("Lookup (ENumberable collection):");
            var asjad = arvud.OrderBy(x => x).ToLookup(x => x, x => x);
            foreach (var x in asjad) Console.WriteLine($"{x.Key} on {x.Count()} tükki");
        }
    }
}
